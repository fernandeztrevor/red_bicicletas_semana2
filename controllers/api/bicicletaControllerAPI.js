var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function (req, res){
    // res.status(200).json({
    //     bicicletas: Bicicleta.allBicis
    // });
    Bicicleta.find({}, function(err, bicicletas){
        res.status(200).json({
            bicicletas:bicicletas
        });
    });
}

exports.bicicleta_create =  function(req, res){
    var bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo});
    bici.ubicacion = [req.body.lat, req.body.lng];

    bici.save(function(err){
        res.status(200).json(bici);
    });
}

exports.bicicleta_delete = function(req,res){
    Bicicleta.removeByCode(req.body.code, function(err){
        if (err) console.log(err);
        res.status(204).send();
    });
}

exports.bicicleta_update = function(req, res){
    var nuevaBici = {
        code: req.body.id,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    };
    var bici = Bicicleta.findOneAndUpdate({code: req.body.id}, nuevaBici, function() {
        res.status(200).json({
            bicicleta: nuevaBici
        });
    })
}