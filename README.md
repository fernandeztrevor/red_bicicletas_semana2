**Segunda entrega entrega**
Trevor Fernandez

**ABM de bicicletas**
http://localhost:3000/bicicletas


DESDE POSTMAN

**BICICLETAS**

**Coleccion de Bicicletas (GET)**
http://localhost:3000/api/bicicletas

**Crear Bicicleta (POST)**
http://localhost:3000/api/bicicletas/create

**Modificar Bicicleta (POST)**
http://localhost:3000/api/bicicletas/update

**Borrar Bicicleta (DELETE)**
http://localhost:3000/api/bicicletas/delete


**USUARIOS**

**Coleccion de Usuarios (GET)**
http://localhost:3000/api/usuarios

**Crear Usuario (POST)**
http://localhost:3000/api/usuarios/create

**Reservar Bicicleta (POST)**
http://localhost:3000/api/usuarios/reservar


TESTS

**Todos los tests**
red_bicicletas_semana2/npm test


**TEST UNITARIOS**

**NOTA: Para realizar cada test unitario es necesario descomentar el BeforeAll del principio**
 

**Test API Bicicletas**
red_bicicletas_semana2/jasmine spec/api/bicicleta_api_test.spec.js

**Test Usuarios**
red_bicicletas_semana2/jasmine spec/models/usuario_test.spec.js

**Test Bicicletas**
red_bicicletas_semana2/jasmine spec/models/bicicleta_test.spec.js

